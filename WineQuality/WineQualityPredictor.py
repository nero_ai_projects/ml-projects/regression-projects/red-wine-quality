import numpy as np
import catboost
import xgboost as xgb


class WineQualityPredictor:
    def __init__(self, catboost_model_path, xgboost_model_path):
        self.catboost_model_path = catboost_model_path
        self.xgboost_model_path = xgboost_model_path
        self.catboost_model = None
        self.xgboost_model = None
        self.load_models()

    def load_models(self):
        # Load CatBoost model
        self.catboost_model = catboost.CatBoost()
        self.catboost_model.load_model(self.catboost_model_path, format="cbm")

        # Load XGBoost model
        self.xgboost_model = xgb.Booster()
        self.xgboost_model.load_model(self.xgboost_model_path)

    def preprocess_data(self, input_data):
        # Preprocess input data
        # NOTE: This method should be implemented according to the preprocessing steps used during model training
        processed_data = np.array(input_data)
        return processed_data

    def predict(self, input_data):
        # Preprocess the data
        processed_data = self.preprocess_data(input_data)

        # Predict with CatBoost
        catboost_predictions = self.catboost_model.predict(processed_data)

        # Predict with XGBoost
        dtest = xgb.DMatrix(processed_data)
        xgboost_predictions = self.xgboost_model.predict(dtest)

        return catboost_predictions, xgboost_predictions
