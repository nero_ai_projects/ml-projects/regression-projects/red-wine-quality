
# Wine Quality Prediction Project

## Overview
This project is dedicated to predicting the quality of wine based on physicochemical properties. It utilizes two powerful machine learning models: CatBoost and XGBoost. The repository includes a Python class for making predictions (`Predictor`), a Jupyter notebook with training procedures (`trained_notebook.ipynb`), and the saved models.

https://www.kaggle.com/datasets/uciml/red-wine-quality-cortez-et-al-2009

## Repository Structure
- `Predictor.py`: A Python class for loading trained models and making predictions.
- `trained_notebook.ipynb`: Jupyter notebook containing the training process of the CatBoost and XGBoost models.
- `catboost_model.cbm`: Saved CatBoost model.
- `xgboost_model.model`: Saved XGBoost model.
- `README.md`: Documentation file (this file).

## Requirements
To run the prediction class, ensure you have the following libraries installed:
- numpy
- catboost
- xgboost

You can install these libraries using pip:
```
pip install numpy catboost xgboost
```

## Usage
To use the `Predictor` class for making predictions, follow these steps:

1. **Import the Class**
   Import the `Predictor` class from `Predictor.py`.

   ```python
   from Predictor import WineQualityPredictor
   ```

2. **Initialize the Predictor**
   Initialize the `WineQualityPredictor` class with the paths to the saved CatBoost and XGBoost models.

   ```python
   predictor = WineQualityPredictor("path_to_catboost_model.cbm", "path_to_xgboost_model.model")
   ```

3. **Prepare Your Input Data**
   Prepare your input data as a list of lists. Each inner list should represent the features of a wine sample.

   Example:
   ```python
   input_data = [[7.4, 0.7, 0, 1.9, 0.076, 11, 34, 0.9978, 3.51, 0.56, 9.4]]
   ```

4. **Make Predictions**
   Use the `predict` method to get predictions. This method returns predictions from both CatBoost and XGBoost models.

   ```python
   catboost_preds, xgboost_preds = predictor.predict(input_data)
   print("CatBoost Predictions:", catboost_preds)
   print("XGBoost Predictions:", xgboost_preds)
   ```

## Note
- The `Predictor` class expects input data in a specific format. Ensure that the data provided for prediction matches the format used during the training phase.
- The paths to the saved models must be correctly specified when initializing the `WineQualityPredictor`.

## Contribution
Feel free to contribute to this project by submitting pull requests or suggesting improvements via issues.

## License
[Specify the license (if applicable)]
